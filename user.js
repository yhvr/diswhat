import fs from "fs";
import path from "path";

export function getUserData(BASE) {
	let user = JSON.parse(
		fs.readFileSync(path.join(BASE, "account", "user.json"))
	);
	
	let cashTotal = user.payments.reduce((a, b) => a + b.amount / 100, 0);
	let friends = user.relationships.filter(r => r.type === 1).length;
	let blocked = user.relationships.filter(r => r.type === 2).length;

	return {
		cashTotal,
		friends,
		blocked,
	};
}
