import { ct, oneOf } from "./padSym.js";
import tkit from "terminal-kit";
import figures from "figures";
const term = tkit.terminal;

import fs from "fs";
import { getMessageData } from "./messages.js";
import { getEventData } from "./events.js";
import { getUserData } from "./user.js";
import path from "path";

import { fileURLToPath } from "url";

export async function getFullReport(BASE) {
	let dataFrom = fs
		.statSync(path.join(BASE, "README.txt"))
		.mtime.toLocaleDateString();

	const {
		totalMessages,
		totalChannels,
		totalChars,
		topEmojis,
		wordBank,
		channelBank,
	} = getMessageData(BASE);
	const { blocked, cashTotal, friends } = getUserData(BASE);
	const eventCount = await getEventData(BASE);

	function trunc(str, len) {
		if (str.length > len)
			return str.substring(0, len - 1) + figures.ellipsis;
		return str;
	}
	function bar() {
		r++;
		term.moveTo(1, r, figures.line.repeat(term.width));
	}
	function title(str) {
		// bar();
		r++;
		term.moveTo(1, r, figures.line + figures.lineUpDownLeft + " ");
		term.bold(str);
		term(" " + figures.lineUpDownRight);
		term(figures.line.repeat(term.width - 5 - str.length));
	}
	function entry(col, sym, before, noticeme, after) {
		r++;
		term.bgColorRgb(...col).black.moveTo(1, r, sym);
		term.bgDefaultColor.moveTo(5, r, before);
		if (typeof noticeme === "number") noticeme = noticeme.toLocaleString();
		term.bold.cyan(noticeme), term(after);
	}
	function ls(list1, list2, ct) {
		r++;
		for (let i = 0; i < ct; i++) {
			r++;
			term.gray(`${i + 1}. `.padStart(4));
			if (typeof list1[i] !== "undefined")
				term(list1[i].toString().padEnd(30));
			else term("<empty>".padEnd(30));
			term("  ");

			term.gray(`${i + 1}. `.padStart(4));
			if (typeof list2[i] !== "undefined")
				term(list2[i].toString().padEnd(30));
			else term("<empty>".padEnd(30));
			term("  ");

			term("\n");
		}
	}

	term.clear();
	let r = 1;
	term.bgColorRgbHex("7289DA").white.moveTo(1, r, " DisWhat ");
	term.bgDefaultColor.white(` by Yhvr | v0.3.0 | data from ${dataFrom}`);

	title("Text");

	r++;
	term.bgColorRgb(255, 127, 127).black.moveTo(
		1,
		r,
		oneOf(
			[
				["BD", 2_000_000],
				["B)", 1_500_000],
				[":O", 1_000_000],
				[":o", 500_000],
				[":3", 250_000],
				[":D", 100_000],
				[":)", 50_000],
				[":-)", 25_000],
				["\\o/", 10_000],
			],
			totalMessages,
			"o/"
		)
	);
	term.bgDefaultColor.moveTo(5, r, "You've sent ");
	term.bold.cyan(totalMessages.toLocaleString());
	term(" messages");

	entry(
		[255, 191, 127],
		ct(".", totalChars, 500_000, 2_500_000),
		"including ",
		totalChars,
		" characters "
	);
	term.italic.gray(`(${(totalChars / totalMessages).toFixed(2)} avg)`);

	entry(
		[255, 255, 127],
		ct("#", totalChannels, 500, 1000),
		"across ",
		totalChannels,
		" channels"
	);

	title("Voice");

	entry(
		[191, 255, 127],
		oneOf(
			[
				[
					figures.lineDownRightArc +
						figures.line +
						figures.lineUpLeftArc,
					250,
				],
				[figures.lineDownRightArc + figures.line, 100],
				[figures.lineDownRightArc, 50],
			],
			eventCount.join_voice_channel,
			" o "
		),
		"You joined ",
		eventCount.join_voice_channel,
		" voice channels"
	);

	entry(
		[127, 255, 127],
		ct("o", eventCount.start_speaking, 2_500, 10_000),
		"in which you spoke ",
		eventCount.start_speaking,
		" times "
	);
	term.italic.gray(
		`(${(eventCount.start_speaking / eventCount.join_voice_channel).toFixed(
			2
		)} avg)`
	);

	title("Loyalty");

	entry(
		[127, 255, 191],
		ct("$", cashTotal, 100, 500),
		"You spent ",
		"$" + cashTotal.toLocaleString("latin", { maximumFractionDigits: 2 }),
		" on Discord"
	);

	entry(
		[127, 255, 255],
		oneOf([["O_O", 50]], eventCount.captcha_solved, "o_o"),
		"and spent approx. ",
		((eventCount.captcha_solved * 10) / 60).toLocaleString("latin", {
			maximumFractionDigits: 2,
		}) + " minutes",
		" solving captchas"
	);

	entry(
		[127, 191, 255],
		ct(">", eventCount.app_opened, 1_000, 5_000),
		"across the ",
		eventCount.app_opened,
		" times you opened the app"
	);

	title("Relations");

	entry([127, 127, 255], "<3 ", "You've made ", friends, " friends");
	entry([191, 127, 255], ">:(", "and blocked ", blocked, " people");

	// title("Guilds");

	// entry([255, 127, 255], "-->", "You joined ", eventCount.guild_joined, " guilds")
	// entry([255, 127, 191], "<--", "and left ", eventCount.leave_guild, "")
	// entry([255, 127, 127], "<->", "now you're in ", eventCount.guild_joined - eventCount.leave_guild, "")

	title("Messages");

	entry(
		[255, 127, 255],
		ct("~", eventCount.message_edited, 250, 1000),
		"You've edited ",
		eventCount.message_edited,
		" messages"
	);
	entry(
		[255, 127, 191],
		ct("!", eventCount.message_deleted, 50, 250),
		"deleted ",
		eventCount.message_deleted
	);
	entry(
		[255, 127, 127],
		oneOf(
			[
				["XD", 25_000],
				["xD", 20_000],
				[":O", 15_000],
				[":o", 10_000],
				[":D", 5_000],
				[":P", 2_500],
				[":)", 1_000],
				["B)", 500],
			],
			eventCount.add_reaction,
			"+"
		),
		"and reacted ",
		eventCount.add_reaction,
		" times"
	);

	title("Favorites");
	r++;

	// fave emojis/words
	let topWords = Object.keys(wordBank).sort(
		(a, b) => wordBank[b] - wordBank[a]
	);
	let commonWords = fs
		.readFileSync(
			path.join(
				fileURLToPath(new URL(".", import.meta.url)),
				"common.txt"
			)
		)
		.toString()
		.toLowerCase()
		.split("\n");
	let lowerWords = topWords.map(w => w.toLowerCase());
	lowerWords = lowerWords
		.filter(w => !commonWords.includes(w))
		.filter(w => !!w);

	term.cyan.bold("    Custom Emojis                       Words\n");
	ls(topEmojis, lowerWords, 10);

	// fave channels/dms
	let channels = {};
	let dms = {};
	for (const id in channelBank) {
		let [chan, ct] = channelBank[id];
		if (chan === null) continue;
		if (chan.startsWith("Direct Message with "))
			dms[chan.substring(20)] = ct;
		else channels[id] = ct;
	}

	// let topChans = Object.keys(wordBank).sort((a, b) => wordBank[b] - wordBank[a]);
	let sortedDMs = Object.keys(dms).sort((a, b) => dms[b] - dms[a]);
	let topDMs = sortedDMs.map(
		d => `@${d} `.padEnd(20) + `(${dms[d].toLocaleString()})`
	);

	let sortedChans = Object.keys(channels).sort(
		(a, b) => channels[b] - channels[a]
	);
	let topChans = sortedChans
		.map(id => JSON.parse(fs.readFileSync(
			path.join(
				BASE,
				"messages",
				"c" + id,
				"channel.json"
			)
		)))
		.filter(info => typeof info.guild !== "undefined")
		.map(
			info => trunc(`#${info.name} - ${info.guild.name} `.padEnd(23), 23) +
				`(${channels[info.id].toLocaleString()})`
		);

	term.cyan.bold("    Friends                             Channels\n");
	ls(topDMs, topChans, 10);

	term("\n");

	// remember that one time?
	let loneWords = topWords.filter(w => wordBank[w] === 1);
	let loner = loneWords[Math.floor(Math.random() * loneWords.length)];

	term("Remember that ");
	term.bold("one time");
	term(' that you said "');
	term.bold(loner);
	term('"?');

	term("\n\nAnyways, that's it for now. See you next time.");

	term("\n");
}
