import fs from "fs";
import csvjson from "csvjson";
import path from "path";

export function getMessageData(BASE) {
	let chatFolders = fs
		.readFileSync(path.join(BASE, "messages", "index.json"))
		.toString();
	chatFolders = JSON.parse(chatFolders);
	let totalChannels = Object.keys(chatFolders).length;

	let totalMessages = 0;
	let totalChars = 0;
	let wordBank = {};
	let messageBank = {};
	let emojiBank = {};
	let channelBank = {};
	Object.keys(chatFolders).forEach(
		c => (channelBank[c] = [chatFolders[c], 0])
	);

	let index = 0;

	for (const folder in chatFolders) {
		// FIXME: Older discord data dumps do not have that "c" prefix before the folder numbers (source: me)
		// If the prefixed path doesn't exist, we should try the unprefixed path.
		// It would probably be worth it to only check for this the first time so we don't waste time checking every single one.
		let csvFile = fs
			.readFileSync(
				path.join(BASE, "messages", "c" + folder, "messages.csv")
			)
			.toString();
		let data = csvjson.toObject(csvFile);

		data.forEach(({ Contents }) => {
			totalMessages++;
			channelBank[folder][1]++;
			totalChars += Contents.length;

			messageBank[Contents] ??= 0;
			messageBank[Contents]++;

			Contents.split(" ").forEach(word => {
				// if (WORD_BLACKLIST.includes(word)) return;
				wordBank[word] ??= 0;
				wordBank[word]++;
			});

			let emojis = [
				...Contents.matchAll(/<:[a-zA-Z0-9]+:[0-9]{12,}>/gimu),
			];
			emojis.forEach(emote => {
				emote = emote.toString();
				emojiBank[emote] ??= 0;
				emojiBank[emote]++;
			});
		});
		index++;
	}

	let commonWords = [["<none>", 0]];
	for (const word in wordBank) {
		const length = word.length;
		const uses = wordBank[word];

		if (commonWords[length] === undefined) {
			commonWords[length] = [word, uses];
			continue;
		}
		if (uses > commonWords[length][1]) commonWords[length] = [word, uses];
	}

	let genningTable = ` Word             | Uses   
----------------------------`;

// 	for (let i = 1; i <= 16; i++) {
// 		break;

// 		genningTable += `
//  ${commonWords[i][0].padEnd(17)}| ${style(commonWords[i][1].toLocaleString(), [
// 			"cyan",
// 		])}`;
// 	}

	let topEmojis = Object.keys(emojiBank)
		.map(emote => [emote, emojiBank[emote]])
		.sort((a, b) => b[1] - a[1]);

	topEmojis = topEmojis.map(e => e[0].match(/:\w+:/));

	return {
		totalMessages,
		totalChannels,
		totalChars,
		wordBank,
		emojiBank,
		topEmojis,
		channelBank,
	};
}
