# `diswhat`

Command-line alternative to DisWho

## Request your data

1. Open Discord
2. User Settings
3. Privacy & Safety
4. Request all of my Data
5. Wait a day or 2 and check your email
6. Unzip

## Install

1. Clone the repository locally, and `cd` into it
2. `npm i -g .` (you may need to `sudo`)


## Usage

`diswhat --help`
`diswhat full <path>`

## Example

`diswhat full Desktop/Datahoard/discord`

Example Output:

```
 DisWhat  by Yhvr | v0.3.0 | data from 11/13/2021
─┤ Text ├───────────────────────────────────────────────────────────────────────────────────────────
:D  You've sent 152,957 messages
... including 3,564,581 characters (23.30 avg)
### across 1,802 channels
─┤ Voice ├──────────────────────────────────────────────────────────────────────────────────────────
 ╭  You joined 59 voice channels
 o  in which you spoke 1,154 times (19.56 avg)
─┤ Loyalty ├────────────────────────────────────────────────────────────────────────────────────────
$$  You spent $219.56 on Discord
o_o and spent approx. 5.83 minutes solving captchas
>>  across the 2,659 times you opened the app
─┤ Relations ├──────────────────────────────────────────────────────────────────────────────────────
<3  You've made 64 friends
>:( and blocked 59 people
─┤ Messages ├───────────────────────────────────────────────────────────────────────────────────────
~~  You've edited 587 messages
!!  deleted 83
:)  and reacted 1,725 times
─┤ Favorites ├──────────────────────────────────────────────────────────────────────────────────────
    Custom Emojis                       Words
 1. :hex:                            1. fart
 2. :LUL:                            2. not
 3. :mfw:                            3. spam
 4. :mmLol:                          4. ok
 5. :pog:                            5. im
 6. :petoday:                        6. it's
 7. :thonk:                          7. its
 8. :trol:                           8. server
 9. :notlikeblob:                    9. because
10. :blobboat1:                     10. yeah
    Friends                             Channels
 1. @[REDACTED]         (4,635)      1. #general - Jacorb's Ga…(17,182)
 2. @[REDACTED]         (2,342)      2. #general - Tree Game   (4,078)
 3. @[REDACTED]         (1,502)      3. #waywo - Tree Game     (3,192)
 4. @[REDACTED]         (1,223)      4. #spam - AD: NG+3 & NG+…(2,911)
 5. @[REDACTED]         (1,220)      5. #intercom - undo       (2,898)
 6. @[REDACTED]         (1,187)      6. #[READCTED]            (2,534)
 7. @[REDACTED]         (771)        7. #[READCTED]            (2,424)
 8. @[REDACTED]         (692)        8. #🤖 - Tree Game        (1,937)
 9. @[REDACTED]         (359)        9. #[READCTED]            (1,727)
10. @[REDACTED]         (351)       10. #mod-chat - Tree Game  (1,640)

Remember that one time that you said ""https://youtu.be/mer5ZkPmX8o"?

Anyways, that's it for now. See you next time.
```
