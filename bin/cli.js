#!/usr/bin/env node

import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import { getFullReport } from "../full.js";

yargs(hideBin(process.argv))
	.scriptName("diswhat")
	.usage("$0 <cmd> [args]")
	.command(
		"full <path>",
		"Get your full report",
		yargs => {
			yargs.positional("path", {
				type: "path",
				describe: "the base path to your Discord data package",
			});
		},
		argv => {
			let path = argv.path;
			if (path.endsWith("/") || path.endsWith("\\")) path = path.slice(0, -1)
			getFullReport(argv.path);
		}
	)
	.version("0.3.0")
	.help().argv;
