import fs from "fs/promises";
import path from "path";

const EVENTS = [
	"join_voice_channel",
	"start_speaking",
	"app_opened",
	"add_reaction",
	"guild_joined",
	"leave_guild",
	"message_edited",
	"message_deleted",
	"channel_opened",
	"captcha_solved",
];

// Okay, this is really funny/stupid. I'll share it. I like putting anecdotes in my code
// I wrote all of this, and it worked like a charm. Then, I got my new data package (6+ yrs)
// and tried to load it, but node... crashed???
// `Error: Cannot create a string longer than 0x1fffffe8 characters`
// TODAY I LEARNED
// Pipes or streams or whatever superiority

export async function getEventData(BASE) {
	const eventCount = {};
	EVENTS.forEach(e => {
		eventCount[e] = 0;
	});

	async function load(id) {
		const name = (await fs.readdir(path.join(BASE, "activity", id)))[0];
		const file = await fs.open(path.join(BASE, "activity", id, name));

		for await (const line of file.readLines()) {
			const data = JSON.parse(line);
			if (EVENTS.includes(data.event_type)) eventCount[data.event_type]++;
		}
	}

	await load("reporting");
	await load("tns");

	return eventCount;
}
